# LorProject4 Readme (2nd draft)

The main purpose of this Pascal scanner program is to scan each token or word and print it out.
We will need to be able to differentiate the Pascal keywords from regular words.
To do this, we will need to identify which keywords Pascal uses and hard code it into the Scanner program.
Once we have identified what keywords we need to have the Scanner exclude, we can build off of that.